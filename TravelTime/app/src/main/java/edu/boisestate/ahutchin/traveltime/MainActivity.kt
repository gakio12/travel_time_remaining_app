package edu.boisestate.ahutchin.traveltime

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil

class MainActivity : SingleFragmentActivity(), TitleFragment.OnFragmentInteractionListener {

    private var REQUEST_ERROR = 0;

    //make this start TitleFragment.
    override fun createFragment(): Fragment {
        return TitleFragment()
    }

    override fun onFragmentInteraction(title: String) {
        supportActionBar!!.title = title
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
    }

    override fun onResume() {
        super.onResume()

        val errorCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)

//        if (errorCode != ConnectionResult.SUCCESS) {
//            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(this, false,
//                                     {
//                                            //Leave if services are unavailable.
//                                            finish()
//                                    })
//        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.menu_main, menu)
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        val id = item.itemId
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true
//        }
//
//        return super.onOptionsItemSelected(item)
//    }
}
