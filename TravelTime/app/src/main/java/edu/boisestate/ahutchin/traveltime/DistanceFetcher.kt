package edu.boisestate.ahutchin.traveltime

import android.app.ProgressDialog
import android.net.Uri
import android.os.AsyncTask
import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class DistanceFetcher {

    val TAG = "DistanceFetcher"

    val API_KEY = "" //TODO: Am I using DistanceMatrix or not?

    var mDistanceObject: DistanceObject? = null

    @Throws(IOException::class)
    fun getUrlBytes(urlSpec: String) : ByteArray {
        val url = URL(urlSpec)
        val connection = url.openConnection() as HttpURLConnection

        try {
            val out = ByteArrayOutputStream()
            val `in` = connection.inputStream
            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                throw IOException(connection.responseMessage +
                        ": with " +
                        urlSpec)
            }
            var bytesRead = 0
            val buffer = ByteArray(1024)
            bytesRead = `in`.read(buffer)
            while ((bytesRead) > 0) {
                out.write(buffer, 0, bytesRead)
                bytesRead = `in`.read(buffer)
            }
            out.close()
            return out.toByteArray()
        } finally {
            connection.disconnect()
        }
    }

    @Throws(IOException::class)
    fun getUrlString(urlSpec: String) : String {
        return String(getUrlBytes(urlSpec))
    }

    fun fetchDistance(start: String?, end: String?): DistanceObject? {
        try {
            var url = Uri.parse("https://maps.googleapis.com/maps/api/distancematrix/json?")
                    .buildUpon()
                    .appendQueryParameter("origins", start)
                    .appendQueryParameter("destinations", end)
                    .appendQueryParameter("key", API_KEY)
                    .build().toString()
            val jsonString = getUrlString(url)
            Log.i(TAG, "url: " + url.toString())
            Log.i(TAG, "Fetched JSON: " + jsonString);
            var jsonBody = JSONObject(jsonString)
            mDistanceObject = parseDistance(jsonBody)
            Log.i(TAG, "Look, distance: " + mDistanceObject?.getDistance())
        } catch (je: JSONException) {
            Log.e(TAG, "Failed to parse JSON", je)
        } catch (e: IOException){
            Log.e(TAG, "Failed to fetch JSON", e)
        }

        return mDistanceObject
    }

    @Throws(IOException::class, JSONException::class)
    private fun parseDistance(jsonBody: JSONObject): DistanceObject {
        var rowsArray = jsonBody.getJSONArray("rows")
        var rowsObject = rowsArray.getJSONObject(0)
        var elementsArray = rowsObject.getJSONArray("elements")
        var elementsObject = elementsArray.getJSONObject(0)
        var distanceJSONobject = elementsObject.getJSONObject("distance")
        var durationJSONobject = elementsObject.getJSONObject("duration")

        var distanceObject = DistanceObject()
        distanceObject.setDistance(distanceJSONobject.getInt("value"))
        distanceObject.setDuration(durationJSONobject.getInt("value"))

        return distanceObject
    }

    fun getDistanceObject(): DistanceObject? {
        return mDistanceObject
    }
}