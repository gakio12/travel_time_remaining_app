package edu.boisestate.ahutchin.traveltime

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import android.Manifest
import android.app.ProgressDialog
import android.os.AsyncTask
import android.text.format.Time
import android.widget.Toast
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import org.json.JSONObject
import java.io.InputStream
import java.net.URL
import edu.boisestate.ahutchin.traveltime.DistanceFetcher
import java.io.IOException
import java.util.*


class TitleFragment : Fragment() {


    private val DIALOG_DATE = "DialogDate"

    private val REQUEST_DATE = 0

    private var mLocationButton: Button? = null
    private var mCalculateButton: Button? = null
    private var mStartDateButton: Button? = null
    private var mOrigin: EditText? = null
    private var mDestination: EditText? = null
    private var mClient: GoogleApiClient? = null
    private var mLocation: Location? = null
    private var mStart: String? = "Boise+ID"
    private var mEnd: String? = "SLC+UT"
    private var mDistanceObject: DistanceObject? = null

    private val TAG = "TitleFragment"

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            //            mParam1 = getArguments().getString(ARG_PARAM1);
            //            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mClient = GoogleApiClient.Builder(activity)
                        .addApi(LocationServices.API)
                        .build()

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(getString(R.string.app_name))
        }
        val v = inflater!!.inflate(R.layout.fragment_title, container, false)

//        var autoCompleteFragment = getFragmentManager().
//                findFragmentById(R.id.place_autocomplete_fragment)

        mOrigin = v.findViewById(R.id.origin_text) as EditText

        mLocationButton = v.findViewById(R.id.location_button) as Button
        mLocationButton!!.setOnClickListener {
            getPerms()
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                getLocation()
            }
        }

        mDestination = v.findViewById(R.id.destination_text) as EditText

        mStartDateButton = v.findViewById(R.id.start_date) as Button
        mStartDateButton!!.setOnClickListener {
            val manager = fragmentManager
            val dialog = DatePickerFragment.newInstance(GregorianCalendar(
                    Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH).getTime())
            dialog.setTargetFragment(this@TitleFragment, REQUEST_DATE)

            dialog.show(manager, DIALOG_DATE)
        }

        mCalculateButton = v.findViewById(R.id.endless_button) as Button
        mCalculateButton!!.setOnClickListener {
            mStart = mOrigin?.getText().toString()
            mEnd = mDestination?.getText().toString()

            Log.i(TAG, "Start: " + mStart)
            val params = arrayOf(mStart, mEnd)
            mDistanceObject = DistanceTask().execute(*params).get()
            Toast.makeText(activity, "Distance: " + mDistanceObject?.getDistance(),
                    Toast.LENGTH_LONG).show()
            //Intent intent = new Intent(getActivity(), GameActivity.class);
            //startActivity(intent);
        }

        return v
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            mListener = activity as OnFragmentInteractionListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement OnFragmentInteractionListener")
        }

    }

    override fun onStart() {
        super.onStart()

        activity.invalidateOptionsMenu()
        mClient!!.connect()
    }

    override fun onStop() {
        super.onStop()

        mClient!!.disconnect()
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html) for
     * more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(title: String)
    }

//    override fun onError(p0: Status?) {
//        throw UnsupportedOperationException()
//    }
//
//    override fun onPlaceSelected(p0: Place?) {
//        mOrigin?.setText(p0.toString())
//        Log.i(TAG, "Place: " + p0?.getName())
//    }

    /**
     * Checks if the device is on SDK 23 or later, if it is, asks for permission for coarse
     * and fine location.
     */
    private fun getPerms() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,
                        arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 0)
            }
        }
    }

    /**
     *  Fetches a balanced (for battery) location of the device using Google Play
     *  Services.
     */
    private fun getLocation() {
        var request = LocationRequest.create()

        request.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
        request.setNumUpdates(1)
        request.setInterval(0)
        LocationServices.FusedLocationApi
                .requestLocationUpdates(mClient, request, {location ->
                        mLocation = location
                        Log.i(TAG, "Got a fix: " + location)

                        mOrigin?.setText(mLocation?.latitude.toString() + "," +
                              mLocation?.longitude.toString())
                })

    }

    private class DistanceTask : AsyncTask<String, Void, DistanceObject>() {

        private val TAG = "TitleFragment"

        override fun doInBackground(vararg p0: String?): DistanceObject? {
            var distanceObject: DistanceObject? = null
            try {
                Log.i(TAG, "Location start: " + p0[0])
                distanceObject = DistanceFetcher().fetchDistance(p0[0], p0[1])
            } catch (e: IOException){
                Log.e(TAG, "FAILED TO FETCH: " + e)
            }

            return distanceObject
        }
    }

}
