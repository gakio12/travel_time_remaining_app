package edu.boisestate.ahutchin.traveltime

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.xml.datatype.Duration

/**
 * Created by gakio on 3/12/2016.
 */
class DistanceObject {
    private var mDistance = 0 //Meters
    private var mDuration = 0 //Seconds
    private var mStatus: Boolean? = false; //I don't know what this is

    fun getDistance(): Int {
        return mDistance
    }

    fun setDistance(distance: Int) {
        mDistance = distance
    }

    fun getDuration(): Int {
        return mDuration
    }

    fun setDuration(duration: Int) {
        mDuration = duration
    }

    override fun toString(): String {
        return mDistance.toString()
    }
}
